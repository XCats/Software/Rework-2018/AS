package org.usfirst.frc.xcats.lib.util.components.actuation.speedcontrol.impl;

import edu.wpi.first.wpilibj.SpeedController;
import org.usfirst.frc.xcats.lib.util.components.actuation.speedcontrol.XSpeedController;
import org.usfirst.frc.xcats.lib.util.components.actuation.speedcontrol.XSpeedControllerStatus;

/**
 * Defines an implementation of {@link XSpeedController} for use with any motor that implements the {@link SpeedController}
 * interface defined in the WPILib
 */
public abstract class XGenericSpeedController<S extends SpeedController> implements XSpeedController {
	protected S motor;
	private String name;
	private boolean inverted;
	private double setPoint;

	private int invertCoefficient = 1;

	protected XGenericSpeedController(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void setInverted() {
		this.inverted = !this.inverted;
		this.invertCoefficient = -1 * this.invertCoefficient;
	}

	public S getMotor() {
		return this.motor;
	}

	public void stop() {
		this.alterSetPoint(0);
	}

	@Override
	public boolean isInverted() {
		return this.inverted;
	}

	@Override
	public void alterSetPoint(double newSetPoint) {
		this.setPoint = newSetPoint;
		motor.set(setPoint * invertCoefficient);
	}

	@Override
	public XSpeedControllerStatus getStatus() {return null;}

	@Override
	public double getSetPoint() {
		return this.setPoint;
	}
}
