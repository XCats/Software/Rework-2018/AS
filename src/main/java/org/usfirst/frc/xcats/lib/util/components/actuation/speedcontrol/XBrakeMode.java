package org.usfirst.frc.xcats.lib.util.components.actuation.speedcontrol;

import com.ctre.phoenix.motorcontrol.NeutralMode;

public enum XBrakeMode {
	BRAKE_MODE, COAST_MODE;

	public NeutralMode toNeutralMode() {
		if(this.equals(XBrakeMode.BRAKE_MODE)) return NeutralMode.Brake;
		else if(this.equals(XBrakeMode.COAST_MODE)) return NeutralMode.Coast;
		else return null;
	}
}
