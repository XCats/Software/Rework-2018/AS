package org.usfirst.frc.xcats.lib.util.components;

/**
 * Defines a set of methods for any mechanism on the robot that is limited, by limit switches or otherwise
 */
public interface XLimitedComponent {
	boolean isAtLowerLimit();
	boolean isAtUpperLimit();
}
