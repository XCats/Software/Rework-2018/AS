package org.usfirst.frc.xcats.robot;

import edu.wpi.first.wpilibj.TimedRobot;
import org.usfirst.frc.xcats.lib.util.XSystemManager;

public class Robot extends TimedRobot {

	//private XSystemManager<PowerupConfig> systemManager = new XSystemManager<>()

    @Override
    public void robotInit() {
    }

    @Override
    public void disabledInit() { }

    @Override
    public void autonomousInit() { }

    @Override
    public void teleopInit() { }

    @Override
    public void testInit() { }

    @Override
    public void disabledPeriodic() { }
    
    @Override
    public void autonomousPeriodic() { }

    @Override
    public void teleopPeriodic() { }

    @Override
    public void testPeriodic() { }
}