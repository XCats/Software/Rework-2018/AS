package org.usfirst.frc.xcats.lib.util.logging;

public class XLogString {

	private String key;
	private String value;

	public XLogString(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public String getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}

}
