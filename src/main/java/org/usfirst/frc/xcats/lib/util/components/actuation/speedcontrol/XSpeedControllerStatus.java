package org.usfirst.frc.xcats.lib.util.components.actuation.speedcontrol;

public class XSpeedControllerStatus {
	public String name;
	public double setPoint;
	public boolean inverted;
}
