package org.usfirst.frc.xcats.lib.util.components.actuation.speedcontrol.impl;

import edu.wpi.first.wpilibj.Talon;

public class XTalonSRX extends XGenericSpeedController<Talon> {

	public XTalonSRX(String name, int pwmChannel) {
		super(name);
		super.motor = new Talon(pwmChannel);
	}
}
