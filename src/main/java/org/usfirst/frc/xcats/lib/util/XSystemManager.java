package org.usfirst.frc.xcats.lib.util;

import java.util.List;

public class XSystemManager<T extends XConfiguration> {
	private final List<XRobotSystem<T>> systems;
	public XSystemManager(List<XRobotSystem<T>> systems) {
		this.systems = systems;
	}

	public void init(T config) {
		systems.stream().forEach(x -> x.init(config));
	}
}
