package org.usfirst.frc.xcats.lib.util.control.impl;

import org.usfirst.frc.xcats.lib.util.components.actuation.speedcontrol.XSpeedController;
import org.usfirst.frc.xcats.lib.util.components.sensors.XSensor;
import org.usfirst.frc.xcats.lib.util.control.XBaseController;
import org.usfirst.frc.xcats.lib.util.control.XController;
import org.usfirst.frc.xcats.lib.util.mutators.XGearbox;

public class XPController extends XBaseController {

	public XPController(XSpeedController motor, XSensor<Double> encoder, double coefficient) {
		super(motor, encoder, new XAlgorithm() {
			@Override
			public void exec() {
				double newSpeed = this.calcP(coefficient, encoder.getValue());
				motor.alterSetPoint(newSpeed);
			}
			private double calcP(double coefficient, double value) {
				return coefficient * (this.setPoint - value);
			}
		});
	}
}
