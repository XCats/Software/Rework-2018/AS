package org.usfirst.frc.xcats.lib.util.command;

import java.util.List;

public interface XRoutine extends XCommand{
	List<XCommand> getCommands();
}
