package org.usfirst.frc.xcats.lib.util.components.outputs;

import org.usfirst.frc.xcats.lib.util.XConfiguration;
import org.usfirst.frc.xcats.lib.util.components.XComponent;

/**
 * A interface that all outputs (LEDs, Logs) should implement
 * @param <T> The configuration type to extend
 * @param <V> The type that the output can display
 */
public interface XOutput<V> extends XComponent {
	void put(V value);
}
