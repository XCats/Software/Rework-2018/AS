package org.usfirst.frc.xcats.lib.util;

public interface XLimitedVoltageDevice {
	double getVoltageThreshold();
	double getVoltageReductionFactor();
}
