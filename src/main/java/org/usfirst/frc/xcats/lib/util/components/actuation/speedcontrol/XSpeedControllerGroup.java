package org.usfirst.frc.xcats.lib.util.components.actuation.speedcontrol;

import java.util.List;

public interface XSpeedControllerGroup<S extends XSpeedController> extends XSpeedController {
	public List<S> getFollowers();
	public S getMaster();
}
