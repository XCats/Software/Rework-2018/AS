package org.usfirst.frc.xcats.lib.util.logging;

import org.usfirst.frc.xcats.lib.util.command.XCommand;
import org.usfirst.frc.xcats.lib.util.sensing.XSensor;
import org.usfirst.frc.xcats.lib.util.subsystems.XSubsystem;

import java.util.ArrayList;

/**
 * Interface that all log output devices should implement
 */
public interface XLogOutput {
	public void write(XLogString out);
	/**
	 * Gets what mechanism (file, terminal, hardware) the log is.
	 * @return The LogType as an enum
	 */
	public LogType getLogType();

	/**
	 * Gets the channels for which the log is registered.
	 * @return An array of the log channels
	 */
	public ArrayList<LogChannel> getLogChannels();

	/**
	 * Adds a channel to the Log
	 * @param channel The channel to be added
	 * @return true if the logChannel was added, false if it was already in the array
	 */
	public boolean addLogChannel(LogChannel channel);

	public enum LogType {
		FILE, TERMINAL, HARDWARE
	}
	public enum LogChannel {
		ERROR, DEBUG, STATUS, STATUS_COMP, ALL
	}
}
