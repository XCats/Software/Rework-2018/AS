package org.usfirst.frc.xcats.lib.util.components.actuation.speedcontrol.impl;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import org.usfirst.frc.xcats.lib.util.components.actuation.speedcontrol.XBrakeMode;
import org.usfirst.frc.xcats.lib.util.components.actuation.speedcontrol.XCANSpeedController;
import org.usfirst.frc.xcats.lib.util.components.actuation.speedcontrol.XSpeedControllerStatus;
import org.usfirst.frc.xcats.lib.util.components.actuation.speedcontrol.impl.XGenericSpeedController;

public class XCANTalonSRX extends XGenericSpeedController<WPI_TalonSRX> implements XCANSpeedController {
	private int channel;

	private XBrakeMode brakeMode;

	public XCANTalonSRX(String name, int channel) {
		super(name);
		this.channel = channel;
		super.motor = new WPI_TalonSRX(channel);
	}

	@Override
	public XSpeedControllerStatus getStatus() {
		return null;
	}

	@Override
	public int getChannel() {
		return this.channel;
	}

	@Override
	public void setBrakeMode(XBrakeMode brakeMode) {
		this.brakeMode = brakeMode;
		super.motor.setNeutralMode(brakeMode.toNeutralMode());
	}

	@Override
	public XBrakeMode getBrakeMode() {
		return this.brakeMode;
	}

	@Override
	public void setRampingRate(double timeToFullSpeed) {
		super.motor.configOpenloopRamp(timeToFullSpeed,0);
	}

	public WPI_TalonSRX getMotor() {
		return this.motor;
	}

}
