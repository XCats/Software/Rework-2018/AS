package org.usfirst.frc.xcats.lib.util.logging;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class XSmartDashOutput extends XBaseLogOutput{
	private static XSmartDashOutput dashOut;

	public XSmartDashOutput() {
		super.logType = LogType.TERMINAL;
	}

	public static XSmartDashOutput getDashOut() {
		if(dashOut == null) dashOut = new XSmartDashOutput();
		return dashOut;
	}

	@Override
	public void write(XLogString s) {
		SmartDashboard.putString(s.getKey(),s.getValue());
	}
}
