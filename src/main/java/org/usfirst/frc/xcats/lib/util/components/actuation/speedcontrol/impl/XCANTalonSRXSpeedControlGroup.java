package org.usfirst.frc.xcats.lib.util.components.actuation.speedcontrol.impl;

import org.usfirst.frc.xcats.lib.util.components.actuation.speedcontrol.XSpeedControllerGroup;
import org.usfirst.frc.xcats.lib.util.components.actuation.speedcontrol.XSpeedControllerStatus;

import java.util.Arrays;
import java.util.List;

public class XCANTalonSRXSpeedControlGroup implements XSpeedControllerGroup<XCANTalonSRX> {

	private String name;
	private XCANTalonSRX master;
	private List<XCANTalonSRX> followers;

	public XCANTalonSRXSpeedControlGroup(String name, XCANTalonSRX master, XCANTalonSRX ... followers) {
		this.name = name;
		this.master = master;
		this.followers = Arrays.asList(followers);
		this.followers.stream().forEach(f -> f.getMotor().follow(this.master.getMotor()));
	}

	@Override
	public List<XCANTalonSRX> getFollowers() {
		return this.followers;
	}

	@Override
	public XCANTalonSRX getMaster() {
		return this.master;
	}

	@Override
	public void setInverted() {
		this.master.setInverted();
	}

	@Override
	public boolean isInverted() {
		return this.master.isInverted();
	}

	@Override
	public void stop() {
		this.alterSetPoint(0);
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void alterSetPoint(double newSetPoint) {
		this.master.alterSetPoint(newSetPoint);
	}

	@Override
	public double getSetPoint() {
		return this.master.getSetPoint();
	}

	@Override
	public XSpeedControllerStatus getStatus() {
		return null;
	}
}
