package org.usfirst.frc.xcats.lib.util;

import org.usfirst.frc.xcats.lib.util.logging.XLoggable;

public interface XRobotSystem<T extends XConfiguration> extends XLoggable {
	public void init(T config);
}
