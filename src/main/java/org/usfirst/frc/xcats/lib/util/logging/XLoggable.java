package org.usfirst.frc.xcats.lib.util.logging;

public interface XLoggable {
	XLogString getStatusString();
}
