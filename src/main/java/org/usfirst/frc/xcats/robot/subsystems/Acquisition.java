package org.usfirst.frc.xcats.robot.subsystems;

import org.usfirst.frc.xcats.lib.util.components.actuation.speedcontrol.impl.XCANTalonSRX;
import org.usfirst.frc.xcats.lib.util.logging.XLogString;
import org.usfirst.frc.xcats.lib.util.subsystems.XSubsystem;
import org.usfirst.frc.xcats.lib.util.XBinaryState;
import org.usfirst.frc.xcats.robot.PowerupConfig;

public class Acquisition implements XSubsystem<PowerupConfig> {
	private XCANTalonSRX LeftMotor;
	private XCANTalonSRX RightMotor;

	private AcquisitionArmState armState;
	private AcquisitionWheelState wheelState;

	private XLogString logState;

	@Override
	public void stop() {
		LeftMotor.alterSetPoint(0);
		RightMotor.alterSetPoint(0);

		wheelState = null;
	}

	@Override
	public void init(PowerupConfig config) {
		this.LeftMotor = new XCANTalonSRX("Left Acquisition Motor", config.L_ACQUISITION_CAN_CHANNEL);
		this.RightMotor = new XCANTalonSRX("Right Acquisition Motor", config.R_ACQUISITION_CAN_CHANNEL);
	}

	@Override
	public XLogString getStatusString() {
		return new XLogString("Acquisition Status:", armState.toString() + " " + wheelState.toString());
	}

	public enum AcquisitionArmState implements XBinaryState {
		OPEN, CLOSED;

		@Override
		public boolean getState() {
			if(this.equals(AcquisitionArmState.OPEN)) return true;
			else if(this.equals(AcquisitionArmState.CLOSED)) return false;
			else {
				throw new IllegalStateException("AcquisitionState does not conform to any known value");
			}
		}

		@Override
		public String toString() {
			if(this.getState()) return "Open";
			return "Closed";
		}
	}

	public enum AcquisitionWheelState implements XBinaryState {
		RUNNING, STOPPED;
		private int speed;

		public void setSpeed(int speed) {
			this.speed = speed;
		}

		@Override
		public boolean getState() {
			if(this.equals(AcquisitionWheelState.RUNNING)) return true;
			else if(this.equals(AcquisitionWheelState.STOPPED)) return false;
			else {
				throw new IllegalStateException("AcquisitionState does not conform to any known value");
			}
		}

		@Override
		public String toString() {
			if(this.getState()) return "Running, Speed: " + speed;
			return "Stopped";
		}
	}



}
