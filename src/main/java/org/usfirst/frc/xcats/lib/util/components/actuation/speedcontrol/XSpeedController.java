package org.usfirst.frc.xcats.lib.util.components.actuation.speedcontrol;

import org.usfirst.frc.xcats.lib.util.components.actuation.XActuator;
import org.usfirst.frc.xcats.lib.util.control.XController;

/**
 * This interface defines a set of methods, most of which were extracted from
 * {@link org.usfirst.frc.xcats.deprecated.XCatsSpeedController}, that define what all speed controllers should
 * do. This interface is hardware agnostic, meaning that for any implementation of a protocol such as CAN or PWM a new
 * interface should be constructed for any hardware-specific features.
 */
public interface XSpeedController extends XActuator, XController {
	/**
	 * Set whether or not the direction the speed controller drives the motor is inverted.
	 */
	public void setInverted();

	/**
	 *
	 * @return whether or not the speed controller's direction is inverted
	 */
	public boolean isInverted();

	public XSpeedControllerStatus getStatus();

}
