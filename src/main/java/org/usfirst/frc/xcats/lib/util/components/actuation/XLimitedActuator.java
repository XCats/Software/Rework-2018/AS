package org.usfirst.frc.xcats.lib.util.components.actuation;

import org.usfirst.frc.xcats.lib.util.XConfiguration;
import org.usfirst.frc.xcats.lib.util.components.XLimitedComponent;

/**
 * An interface that all limited actuators (Linear, pneumatic, etc) should implement
 */
public interface XLimitedActuator extends XActuator, XLimitedComponent {
}
