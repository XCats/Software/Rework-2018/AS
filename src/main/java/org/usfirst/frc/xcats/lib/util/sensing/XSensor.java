package org.usfirst.frc.xcats.lib.util.sensing;

import org.usfirst.frc.xcats.lib.util.XRobotSystem;
import org.usfirst.frc.xcats.lib.util.logging.XLoggable;

public interface XSensor extends XLoggable, XRobotSystem {
}
