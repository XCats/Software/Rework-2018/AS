package org.usfirst.frc.xcats.lib.util.components.actuation;

import org.usfirst.frc.xcats.lib.util.XConfiguration;
import org.usfirst.frc.xcats.lib.util.components.XComponent;

/**
 * The interface all actuators (Pneumatics, motors, etc) should implement
 */
public interface XActuator extends XComponent {
	void stop();
}
