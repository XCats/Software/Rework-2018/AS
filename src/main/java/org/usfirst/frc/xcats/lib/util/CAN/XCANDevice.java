package org.usfirst.frc.xcats.lib.util.CAN;

public interface XCANDevice {
	int getChannel();
}
