package org.usfirst.frc.xcats.lib.util.command;

import org.usfirst.frc.xcats.lib.util.XRobotSystem;
import org.usfirst.frc.xcats.lib.util.logging.XLoggable;

public interface XCommand extends XLoggable {
	void start();
	void cancel();

	boolean isCompleted();
}
