package org.usfirst.frc.xcats.lib.util.components;

import org.usfirst.frc.xcats.lib.util.XConfiguration;

public interface XConfigurable<T extends XConfiguration> {
	void init(T config);
}
