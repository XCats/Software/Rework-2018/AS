package org.usfirst.frc.xcats.lib.util.components.sensors;

import org.usfirst.frc.xcats.lib.util.XConfiguration;
import org.usfirst.frc.xcats.lib.util.components.XComponent;

/**
 * An interface that all sensors should implement
 * @param <V> The type of the value that the sensor returns
 */
public interface XSensor<V> extends XComponent {
	V getValue();
	void zero();
}
