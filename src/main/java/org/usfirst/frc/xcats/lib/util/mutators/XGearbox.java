package org.usfirst.frc.xcats.lib.util.mutators;

public interface XGearbox {
	Double getRatio();
	Double getMaxSpeed();
	Double getMaxTorque();
}
