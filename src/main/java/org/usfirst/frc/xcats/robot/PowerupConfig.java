package org.usfirst.frc.xcats.robot;

import org.usfirst.frc.xcats.lib.util.XConfiguration;
import org.usfirst.frc.xcats.lib.util.components.sensors.XSensor;

public class PowerupConfig implements XConfiguration {
	public final int L_ACQUISITION_CAN_CHANNEL = 30;//CAN id for left acquisition motor
	public final int R_ACQUISITION_CAN_CHANNEL = 31;//CAN id for right acquisition motor
}
