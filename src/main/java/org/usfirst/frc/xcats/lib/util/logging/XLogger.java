package org.usfirst.frc.xcats.lib.util.logging;

import org.usfirst.frc.xcats.lib.util.command.XCommand;
import org.usfirst.frc.xcats.lib.util.subsystems.XSubsystem;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Manages Logs and writing to them
 */
public class XLogger {
	//We only want one logger to be able to exist at any given time, so we have a static logInstance
	private static XLogger logInstance;

	//We hold all the outputs in a list so they can be enabled and disabled
	public List<XLogOutput> logOutputs;
	private ArrayList<XLogOutput.LogChannel> channels;

	//Define an empty constructor
	public XLogger() {

	}

	/**
	 * Gets the XLogger Singleton, and creates it if it doesn't exist yet
	 * @return the XLogger Singleton
	 */
	public static synchronized XLogger getInstance() {
		if(XLogger.logInstance == null) XLogger.logInstance = new XLogger();
		return XLogger.logInstance;
	}

	/**
	 * Removes a logOutput from the Logger's list of outputs
	 * @param toRemove
	 */
	public void removeLogOutput(XLogOutput toRemove) {
		logOutputs.remove(toRemove);
	}

	public void addLogOutput(XLogOutput toAdd) {
		logOutputs.add(toAdd);
	}

	/**
	 * Writes an XLoggable to all possible log objects
	 * @param toLog the Loggable to write to the log
	 */
	public void writeToLogs(XLogString toLog) {
		logOutputs.stream().forEach(x -> x.write(toLog));
	}

	/**
	 * Writes an XLoggabble to all log objects of a specified LogType
	 * @param logTypes
	 * @param toLog
	 */
	public void writeToLogs(XLogString toLog, ArrayList<XLogOutput.LogType> logTypes) {
		logOutputs.stream().filter(x -> logTypes.contains(x.getLogType())).forEach(x -> x.write(toLog));
	}

	public void writeToChannels(XLogString toLog, ArrayList<XLogOutput.LogChannel> logChannels) {
		//logOutputs.stream().filter();
	}
}
