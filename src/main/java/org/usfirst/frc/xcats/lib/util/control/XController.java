package org.usfirst.frc.xcats.lib.util.control;

public interface XController {
	public void alterSetPoint(double newSetPoint);
	public double getSetPoint();
}
