/**
 * Contains all of the classes from the software-2018 repository. All classes should have the {@link java.lang.Deprecated}
 * annotation. This is all legacy code that should be reimplemented. Interfaces need to be extracted and a general code
 * style needs to be developed.
 */
package org.usfirst.frc.xcats.deprecated;