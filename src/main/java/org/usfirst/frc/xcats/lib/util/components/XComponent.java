package org.usfirst.frc.xcats.lib.util.components;

import org.usfirst.frc.xcats.lib.util.XConfiguration;

/**
 * An interface that all Sensors, Actuators, and Outputs should Implement
 */
public interface XComponent {
	String getName();
}
