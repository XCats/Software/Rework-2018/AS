package org.usfirst.frc.xcats.lib.util.control;

import org.usfirst.frc.xcats.lib.util.components.actuation.speedcontrol.XSpeedController;
import org.usfirst.frc.xcats.lib.util.components.sensors.XSensor;
import org.usfirst.frc.xcats.lib.util.control.impl.XAlgorithm;
import org.usfirst.frc.xcats.lib.util.mutators.XGearbox;

public abstract class XBaseController implements XController {

	protected XSpeedController motor;
	protected XSensor<Double> encoder;
	private XAlgorithm algorithm;

	private double setPoint;
	private double coefficient = 1;

	public XBaseController(XSpeedController motor, XSensor<Double> encoder, XAlgorithm algorithm) {
		this.motor = motor;
		this.encoder = encoder;
		this.algorithm = algorithm;
	}

	public void run() {
		Thread t = new Thread(algorithm);
		t.start();
	}

	@Override
	public void alterSetPoint(double newSetPoint) {
		this.setPoint = newSetPoint;
	}

	@Override
	public double getSetPoint() {
		return this.setPoint;
	}
}
