package org.usfirst.frc.xcats.lib.util.control.impl;

public abstract class XAlgorithm implements Runnable{
	public double setPoint;
	public double currentValue;

	public void run() {
		while(!Thread.interrupted()) exec();
	}

	public abstract void exec();
}
