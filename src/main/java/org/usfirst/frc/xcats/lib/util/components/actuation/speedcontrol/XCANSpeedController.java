package org.usfirst.frc.xcats.lib.util.components.actuation.speedcontrol;

import org.usfirst.frc.xcats.lib.util.CAN.XCANDevice;
import org.usfirst.frc.xcats.lib.util.components.actuation.speedcontrol.XBrakeMode;
import org.usfirst.frc.xcats.lib.util.components.actuation.speedcontrol.XSpeedController;

public interface XCANSpeedController extends XCANDevice, XSpeedController {
	void setBrakeMode(XBrakeMode brakeMode);
	XBrakeMode getBrakeMode();
	void setRampingRate(double timeToFullSpeed);
}
