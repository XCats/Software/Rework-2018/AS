package org.usfirst.frc.xcats.lib.util.logging;

public abstract class XBaseLogOutput implements XLogOutput {
	protected LogType logType;

	@Override
	public LogType getLogType() {
		return this.logType;
	}

	@Override
	public void logObject(XLoggable toLog) {
		this.write(toLog.getStatusString());
	}

	public abstract void write(XLogString s);
}
