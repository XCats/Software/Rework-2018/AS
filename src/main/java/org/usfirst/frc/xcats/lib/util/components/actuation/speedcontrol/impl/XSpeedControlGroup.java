package org.usfirst.frc.xcats.lib.util.components.actuation.speedcontrol.impl;

import org.usfirst.frc.xcats.lib.util.components.actuation.speedcontrol.XSpeedController;
import org.usfirst.frc.xcats.lib.util.components.actuation.speedcontrol.XSpeedControllerStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class XSpeedControlGroup implements XSpeedController {

	private String name;
	private XSpeedController master;
	private List<XSpeedController> followers;

	private double setPoint;
	private boolean inverted;
	private double invertCoefficient;

	public XSpeedControlGroup(String name, XSpeedController master, XSpeedController... followers) {
		this.name = name;
		this.master = master;
		this.followers = Arrays.asList(followers);
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public void alterSetPoint(double newSetPoint) {
		this.setPoint = newSetPoint;
		master.alterSetPoint(this.setPoint);
		followers.forEach(s -> s.alterSetPoint(this.setPoint));
	}

	@Override
	public double getSetPoint() {
		return this.setPoint;
	}

	@Override
	public void setInverted() {
		this.inverted = !this.inverted;
		this.invertCoefficient = -1 * this.invertCoefficient;
		master.setInverted();
		followers.forEach(s -> s.setInverted());
	}

	@Override
	public boolean isInverted() {
		return this.inverted;
	}

	public List<XSpeedController> getFollowers(){
		return this.followers;
	}

	public XSpeedController getMaster() {
		return this.master;
	}

	@Override
	public void stop() {
		this.alterSetPoint(0);
	}

	@Override
	public XSpeedControllerStatus getStatus() {return null;}
}
