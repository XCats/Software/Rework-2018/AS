package org.usfirst.frc.xcats.lib.util.subsystems;

import org.usfirst.frc.xcats.lib.util.XConfiguration;
import org.usfirst.frc.xcats.lib.util.XRobotSystem;
import org.usfirst.frc.xcats.lib.util.logging.XLoggable;

public interface XSubsystem<T extends XConfiguration> extends XRobotSystem<T> {
	void stop();
}
